﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3B
{
    class EmpleadoFijo : Empleado
    {
        public DateTime AñoDeEntrada { get; set;}
        static private float sueldomensual = 500;
        public int mesesTrabajados { get; set; }



        public EmpleadoFijo(DateTime AñoDeEntrada, string apellidos, string nombres, int edad, string departamento) : base(apellidos, nombres, edad, departamento)
        {
            this.AñoDeEntrada = AñoDeEntrada;
        }

        // and setters
        //metodo............

        public float Calcularsueldo()
        {
            float sueldo = mesesTrabajados * sueldomensual;
            return sueldo;
        }
    }
}
