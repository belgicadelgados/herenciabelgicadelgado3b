﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3B
{
    class PersonaDeServicio : Empleados
    {
        public string seccion { get; set; }
        public PersonaDeServicio(string seccion, int despacho, DateTime Incorporacion, string Nombre, string Apellido, string Cedula, string EstadoCivil) : base(despacho, Incorporacion, Nombre, Apellido, Cedula, EstadoCivil)
        {
            this.seccion = seccion;
        }
        public void trasladodeseccion(string seccion)
        {
            this.seccion = seccion;
        }
        public void Mostrardatos()
        {
            Console.WriteLine("Datos del empleado:\nNombre y Apellido: " + Nombre + " " + Apellido + "\n" +
            "Nº cedula: " + Cedula + "\n" +
            "Estado civil: " + EstadoCivil + "\n" +
            "Fecha de ingreso: " + Incorporacion + "\n" +
             "Sección: " + seccion);
        }
    }
}
