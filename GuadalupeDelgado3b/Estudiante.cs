﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3B
{
    class Estudiante : Persona

    {
        public int Curso{get;set;}
        public Estudiante (int Curso, string Nombre, string Apellido, string Cedula, string EstadoCivil) : base(Nombre, Apellido, Cedula, EstadoCivil)
        {
            this.Curso = Curso;
        }
        public void matriculacion(int Curso)
        {
           this.Curso = Curso;
        }
        public void Mostrardatos()
        {
            Console.WriteLine("Datos del empleado:\nNombre y Apellido: " + Nombre + " " + Apellido + "\n" +
            "Nº cedula: " + Cedula + "\n" +
            "Estado civil: " + EstadoCivil + "\n" +
            "Curso: " + Curso + "\n");
        }
    }
}
