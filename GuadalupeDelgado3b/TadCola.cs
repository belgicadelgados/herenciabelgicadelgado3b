﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3b
{
    class TadCola
    {
        public static int tamano = 8;
        public int[] cola = new int[tamano];
        public int primero = -1;
        public int ultimo = -1;

        public Boolean Vacia()
        {
            return (primero == -1 & ultimo == -1);
        }
        public void Encolar(int dato)
        {
            if (Vacia())
            {
                primero++;
                ultimo++;
                cola[ultimo] = dato;
            }
        }

    public int Desencolar()
    {
        int valor = cola[primero];
        primero++;
        if (primero == tamano)
        {
            primero = 0;
        }

        return valor;
    }

    public int Ultimo()
    {
        return cola[ultimo];
    }
}
}
