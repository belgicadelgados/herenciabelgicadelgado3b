﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3B
{
    class EmpleadosPorHora:Empleado

    {
        static private float precioDeHora = 2.50f;
        public int horasTrabajadas { get; set; }

        public EmpleadosPorHora(int horasTrabajadas, string apellidos, string nombres, int Edad, string Departamento) : base(apellidos, nombres, Edad, Departamento)
        {
            this.horasTrabajadas = horasTrabajadas;
        }
        //metodo............

        public float Calcularsueldo()
        {
            float sueldo = precioDeHora*horasTrabajadas;
            return sueldo;
        }
    }
}
