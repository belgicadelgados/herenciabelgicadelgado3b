﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3B
{
    class Program
    {
        static void Main(string[] args)
        {
            string datosEmpleado = "";


            EmpleadoFijo empleadoFijo = new EmpleadoFijo(new DateTime(1991, 03, 10), "delgado", "Belgica", 46, "Limpieza");

            datosEmpleado = "Nombre: " + empleadoFijo.Nombres + "\nApellidos: " + empleadoFijo.Apellidos + "\nSueldo:" + empleadoFijo.Calcularsueldo();
            Console.WriteLine(datosEmpleado);
            Console.WriteLine("\n***************\n");

            EmpleadoTemporal empleadoTemporal = new EmpleadoTemporal(new DateTime(2008, 5, 1, 8, 30, 52), new DateTime(2009, 5, 1, 8, 30, 52), "delgado", "Belgica", 46, "Limipieza");
            datosEmpleado = "Nombre: " + empleadoTemporal.Nombres+ "\nApellidos: " + empleadoTemporal.Apellidos + "\nSueldo:" + empleadoTemporal.CalcularSueldo();
            Console.WriteLine(datosEmpleado); Console.WriteLine("\n***************\n");
            Estudiante estudiante = new Estudiante(1,"Said","Delgado","1312963455","Soltero");
            Profesor profesor = new Profesor("Matematicas", 2, new DateTime(2008, 5, 1, 8, 30, 52), "Said", "Delgado", "1312963455", "Soltero");
            PersonaDeServicio personaDeServicio = new PersonaDeServicio("bliblioteca", 2, new DateTime(2008, 5, 1, 8, 30, 52), "Said", "Delgado", "1312963455", "Soltero");
            estudiante.Mostrardatos();
            profesor.Mostrardatos();
            personaDeServicio.Mostrardatos();
        }
    }
}
