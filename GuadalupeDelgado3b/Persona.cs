﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3B
{
    class Persona
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cedula { get; set; }
        public string EstadoCivil { get; set; }

        public Persona(string Nombre, string Apellido,string Cedula, string EstadoCivil)
        {
            this.Nombre = Nombre;
            this.Apellido = Apellido;
            this.Cedula = Cedula;
            this.EstadoCivil = EstadoCivil;
        }
        public void cambiodeestadocivil (string estadocivil)
        {
            EstadoCivil = estadocivil;
        }
    }
}
