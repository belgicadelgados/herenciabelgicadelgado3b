﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3B
{
    class EmpleadoTemporal : Empleado
    {
        public int Id;
        public DateTime FechaDeIngreso
        {
            get; set;
        }
        public DateTime FechaDeSalida
        {
            get; set;
    }
        private int sueldo = 500;

        public EmpleadoTemporal(DateTime FechaDeIngreso, DateTime FechaDeSalida, string apellidos, string nombres, int Edad, string Departamento) : base(apellidos, nombres, Edad, Departamento)
        {
            this.FechaDeIngreso = FechaDeIngreso;
            this.FechaDeSalida = FechaDeSalida;
        }
        //METODOS

        public float CalcularSueldo()
        {
            TimeSpan diferenciaDeFechas = FechaDeSalida - FechaDeIngreso;
            int dias = diferenciaDeFechas.Days;
            int meses = dias / 30;
            float totalSueldo = meses * sueldo;
            return totalSueldo;
        }
    }
}
