﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3B
{
    class Profesor: Empleados
    {
        public string Departamento { get; set; } 
        public Profesor ( string Departamento, int despacho, DateTime Incorporacion, string Nombre, string Apellido, string Cedula, string EstadoCivil) : base(despacho, Incorporacion, Nombre, Apellido, Cedula, EstadoCivil)
        {
            this.Departamento = Departamento;
        }
        public void Cambiodedepartamento(string departamento)
        {
            Departamento = departamento;
        }
        public void Mostrardatos()
        {
            Console.WriteLine("Datos del empleado:\nNombre y Apellido: " + Nombre + " " + Apellido + "\n" +
            "Nº cedula: " + Cedula + "\n" +
            "Estado civil: " + EstadoCivil + "\n" +
            "Fecha de ingreso: " + Incorporacion + "\n" +
             "Departamento: " + Departamento);
        }
    }
}
