﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuadalupeDelgado3b
{
    class TadPila
    {
        public static int tamano = 6;
        public static int[] pila = new int[tamano];
        public static int cima = -1;

        public Boolean Vacia()
        {
            return (pila.Length - 1) == cima;
        }
        public void Apilar(int dato)
        {
            if (cima < tamano)
            {
                cima++;
                pila[cima] = dato;
            }

        }

        public int Desapilar()
        {
            int valor = Top();
            cima--;
            return valor;
        }

        public int Top()
        {
            return pila[cima];
        }
    }
}
